<?php
/**
 * Shoptimizer child theme functions
 *
 * @package shoptimizer
 */

use PluginContainer\Packages\WcUtilityPack\Model\Product;

/**
 * Loads parent and child theme scripts.
 */
function shoptimizer_child_enqueue_scripts()
{
    $parent_style = 'shoptimizer-style';
    $parent_base_dir = 'shoptimizer';
    wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css', [],
        wp_get_theme($parent_base_dir) ? wp_get_theme($parent_base_dir)->get('Version') : '');

    if (is_rtl()) {
        wp_enqueue_style('shoptimizer-rtl', get_template_directory_uri() . '/rtl.css', [],
            wp_get_theme($parent_base_dir) ? wp_get_theme($parent_base_dir)->get('Version') : '');
    }

    wp_enqueue_style('shoptimizer-child-style', get_stylesheet_directory_uri() . '/style.css', [$parent_style],
        wp_get_theme()->get('Version'));
}

add_action('wp_enqueue_scripts', 'shoptimizer_child_enqueue_scripts');


add_action('woocommerce_after_add_to_cart_button', 'addToAnyShareProductPage');

function addToAnyShareProductPage()
{
    echo do_shortcode('[addtoany]');
}

//Override function for a single string
function shoptimizer_header_cart_drawer()
{
//    $shoptimizer_cart_title = shoptimizer_get_option( 'shoptimizer_cart_title' );
    $shoptimizer_cart_title = 'Korpa';
    if (shoptimizer_is_woocommerce_activated()) {
        if (is_cart()) {
            $class = 'current-menu-item';
        } else {
            $class = '';
        }
        ?>
        <div class="shoptimizer-mini-cart-wrap">
            <div id="ajax-loading">
                <div class="shoptimizer-loader">
                    <div class="spinner">
                        <div class="bounce1"></div>
                        <div class="bounce2"></div>
                        <div class="bounce3"></div>
                    </div>
                </div>
            </div>
            <div class="cart-drawer-heading"><?php
                echo shoptimizer_safe_html($shoptimizer_cart_title); ?></div>
            <div class="close-drawer"></div>
            <?php
            the_widget('WC_Widget_Cart', 'title='); ?>
        </div>
        <?php

        $shoptimizer_cart_drawer_js = '';
        $shoptimizer_cart_drawer_js .= "
				jQuery( document ).ready( function( $ ) {
					$( 'body' ).on( 'added_to_cart', function( event, fragments, cart_hash ) {
						$( 'body' ).addClass( 'drawer-open' );
					} );				
				} );
				document.addEventListener( 'DOMContentLoaded', function() {
					document.addEventListener( 'click', function( event ) {
						var is_inner = event.target.closest( '.shoptimizer-mini-cart-wrap' );
						if ( ! event.target.classList.contains( 'shoptimizer-mini-cart-wrap' ) && ! is_inner ) {
							document.querySelector( 'body' ).classList.remove( 'drawer-open' );
						}
						var is_inner2 = event.target.closest( '.cart-click' );
						if ( event.target.classList.contains( 'cart-click' ) || is_inner2 ) {
							var is_header = event.target.closest( '.site-header-cart' );
							if ( is_header ) {
								event.preventDefault();
								document.querySelector( 'body' ).classList.toggle( 'drawer-open' );
							}
						}
						if ( event.target.classList.contains( 'close-drawer' ) ) {
							document.querySelector( 'body' ).classList.remove( 'drawer-open' );
						}
					} );
				} );
				var interceptor = ( function( open ) {
					XMLHttpRequest.prototype.open = function( method, url, async, user, pass ) {
						this.addEventListener( 'readystatechange', function() {
						switch ( this.readyState ) {
							case 1:
								document.querySelector( '#ajax-loading' ).style.display = 'block';
							break;
							case 4:
								document.querySelector( '#ajax-loading' ).style.display = 'none';
							break;
						}
						}, false );
						if ( async !== false ) {
							async = true;
						}
						open.call( this, method, url, async, user, pass );
					};
				}  ( XMLHttpRequest.prototype.open ) );
				document.addEventListener( 'DOMContentLoaded', function() {
					document.querySelector( '#ajax-loading' ).style.display = 'none';
				} );
				";

        wp_add_inline_script('shoptimizer-main', $shoptimizer_cart_drawer_js);
    }
}

add_action('wp_head', 'GAscript');
function GAscript()
{
    echo '<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-8N7DNG35JD"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag("js", new Date());

  gtag("config", "G-8N7DNG35JD");
</script>
';
}
if (defined('USE_ELASTIC') && USE_ELASTIC) {
    add_action('init', function (){
        remove_action( 'woocommerce_shop_loop_item_title', 'shoptimizer_loop_product_title', 10 );
        remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open');
        add_action('woocommerce_shop_loop_item_title', '_shoptimizer_loop_product_title', 10);
        remove_action('woocommerce_before_shop_loop_item_title','shoptimizer_template_loop_image_link_open',5);
    });
}

function _shoptimizer_loop_product_title()
{
    /**
     * @var $product Product
     */
    global $product;
    if ($product) {
        if($product instanceof Product) {
            $categories = $product->get_categories();
        } else {
            $categories = wc_get_product_category_list($product->get_id());
            echo '<p class="product__categories">' . $categories .'</p>';
            echo '<div class="woocommerce-loop-product__title"><a href="' . $product->get_permalink() . '" aria-label="' . $product->get_title() . '" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">' . $product->get_title() . '</a></div>';
            return;
        }
        $shoptimizer_layout_woocommerce_display_category = shoptimizer_get_option('shoptimizer_layout_woocommerce_display_category');
        ?><?php
        if (true === $shoptimizer_layout_woocommerce_display_category): ?>
            <p class="product__categories">
            <?php
            foreach ($categories as $key => $category): $comma = '' ?><?php
                if ($key !== array_key_last($categories)) $comma = ',' ?>
                <a href="<?=$category['permalink']?>" rel="tag"
                   title="<?=$category['name']?>"><?=$category['name']?><?=$comma?></a>
            <?php
            endforeach; ?>
            <?='</p>'?><?php
        endif; ?><?php
        echo '<div class="woocommerce-loop-product__title"><a href="' . $product->get_permalink() . '" aria-label="' . htmlentities($product->get_title()) . '" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">' . htmlentities($product->get_title()) . '</a></div>';
    }
}


//Remove filters on mobile
add_action( 'wp', 'shoptimizerRemoveMobileFilter', 90 );
function shoptimizerRemoveMobileFilter() {
    remove_action( 'woocommerce_after_shop_loop', 'shoptimizer_mobile_filters', 5 );
    remove_action( 'woocommerce_before_shop_loop', 'shoptimizer_mobile_filters', 5 );
}
function removeCategoryHierarchyFromUrl($args) {
    $args['rewrite']['hierarchical'] = false;
    return $args;
}
//add_filter( 'woocommerce_taxonomy_args_product_cat', 'removeCategoryHierarchyFromUrl' );

add_filter('sliderBeforeItemTitle', function ($product)
{
    echo '<a title="' . $product->get_title() . '" href="' . $product->get_permalink() . '">';
    echo woocommerce_get_product_thumbnail();
    echo '</a>';
    echo '<div>';
},9);

// Print sale date on single pages
add_filter('woocommerce_get_price_html', 'printSaleDateRange', 100, 2);
function printSaleDateRange($price, $product)
{
    if (is_single() && $product->is_on_sale()) {
        $untilOutOfStock = $product->get_meta('untilOutOfStock');
        if($untilOutOfStock) {
            $price = __('Dok traju zalihe','plugin-container');
        } else {
            $sales_price_from = get_post_meta($product->get_id(), '_sale_price_dates_from', true);
            $sales_price_to = get_post_meta($product->get_id(), '_sale_price_dates_to', true);
            $fromText = '';
            $toText = '';
            if($sales_price_from !== '') {
                $sales_price_date_from = date("j-m-Y", $sales_price_from);
                $fromText = __('od ','plugin-container') . $sales_price_date_from;
            }
            if($sales_price_to !== '') {
                $sales_price_date_to = date("j-m-Y", $sales_price_to);
                $toText = __('do ','plugin-container') . $sales_price_date_to;
            }
            $price = str_replace(
                '</ins>',
                ' </ins> <b style="display:block;    ">(Akcija traje ' . $fromText . ' ' . $toText . ')</b>',
                $price
            );
        }
    }

    return apply_filters('woocommerce_get_price', $price);
}
// Checkbox for printing "until out of stock" instead of the sale date
add_action('woocommerce_product_options_general_product_data', static function ()
{

    $postId = $_GET['post'] ?? '';
    $isInputChecked = get_post_meta($postId, 'untilOutOfStock', true)  ? 'checked' : '';
    $untilOutOfStockText= __('Dok traju zalihe','plugin-container');
    echo <<<html
                <p class="form-field">     
                    <label for="untilOutOfStock">$untilOutOfStockText</label>
                    <input $isInputChecked name="untilOutOfStock" id="untilOutOfStock" type="checkbox">
                </p>         
            html;
});

add_action('woocommerce_process_product_meta', static function ($postId)
{
    $untilOutOfStock = $_POST['untilOutOfStock'];

    if($untilOutOfStock) {
        update_post_meta($postId, 'untilOutOfStock', true);
    } else {
        delete_post_meta($postId, 'untilOutOfStock');
    }
});
add_image_size( 'shop_catalog', 300, 300, false);

add_action('wp_head', 'addScriptToHead');
function addScriptToHead(){
    //Close PHP tags ?>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-237471822-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-237471822-1');
    </script>
    <?php //Open PHP tags
}


