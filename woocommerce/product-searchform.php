<?php
/**
 * The template for displaying product search form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/product-searchform.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.3.0
 */

if (!defined('ABSPATH')) {
    exit;
}

?>
<form role="search" method="get" class="woocommerce-product-search" action="<?php
echo esc_url(home_url('/')); ?>">
    <label class="screen-reader-text" for="woocommerce-product-search-field-<?php
    echo isset($index) ? absint($index) : 0; ?>"><?php
        esc_html_e('Search for:', 'woocommerce'); ?></label>
    <input type="search" id="woocommerce-product-search-field-<?php
    echo isset($index) ? absint($index) : 0; ?>" class="search-field" placeholder="<?php
    echo esc_attr__('Search products&hellip;', 'woocommerce'); ?>" value="<?php
    echo get_search_query(); ?>" name="s"/>
    <button type="submit" value="<?php
    echo esc_attr_x('Search', 'submit button', 'woocommerce'); ?>"><?php
        echo esc_html_x('Search', 'submit button', 'woocommerce'); ?></button>
    <input type="hidden" name="post_type" value="product"/>
    <?php
    $catName = '';
    $catId = '';
    $queriedObj = get_queried_object(); 
    if ($queriedObj instanceof WP_Term && ($queriedObj->taxonomy === 'product_cat')) {
        $catName = ucfirst(mb_strtolower($queriedObj->name));
        $catId = $queriedObj->term_id;
    }
    if (isset($_GET['specificSearch']) && $_GET['specificSearch'] !== '0') {
        $catId = $_GET['specificSearch'];
        $catName = ucfirst(mb_strtolower(get_term_by('term_id', $catId, 'product_cat')->name));
        add_action('woocommerce_before_main_content', function () use ($catName) {
            if (!defined('DID_CAT_NAME_ACTION')) {
                echo '<h1 class="woocommerce-products-header__title page-title">'.mb_strtoupper($catName).'</h1>';
                define('DID_CAT_NAME_ACTION', true);
            }
        }, 99);
    }
    if ($catId !== '' && $catName !== ''):?>
        <label>
            <?=$catName?>
            <input checked type="radio" name="specificSearch" value="<?=$catId?>">
        </label><label>
            <?=__('Pretraga celog sajta', 'plugin-container')?>
            <input type="radio" name="specificSearch" value="0">
        </label>
    <?php
    endif; ?>
</form>

